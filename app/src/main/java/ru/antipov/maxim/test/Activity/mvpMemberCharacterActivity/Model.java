package ru.antipov.maxim.test.Activity.mvpMemberCharacterActivity;

import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.test.Parsers.CharacterMember;
import ru.antipov.maxim.test.Parsers.CharacterName;
import ru.antipov.maxim.test.Parsers.HouseName;
import ru.antipov.maxim.test.R;
import ru.antipov.maxim.test.Singleton.Singleton;
import ru.antipov.maxim.test.Singleton.SingletonNetwork;
import ru.antipov.maxim.test.Utils.Utils;

public class Model {
    public interface LoadCallback {
        void onLoad();
        void onError(String error);
    }

    private CharacterMember mCharacterMember;
    private String numberMember;
    private String numFather;
    private String numMother;
    private String numSpouse;
    private String nameFather;
    private String nameMother;
    private String nameSpouse;
    private ArrayList<String> numberHouse;
    private ArrayList<HouseName> houseName;


    Model(String numberMember) {
        this.numberMember = numberMember;
        this.numberHouse  = new ArrayList<>();
        this.houseName    = new ArrayList<>();
    }

    public CharacterMember getCharacterMember() {
        return mCharacterMember;
    }

    public void setCharacterMember(CharacterMember characterMember) {
        mCharacterMember = characterMember;
    }

    public String getNumberMember() {
        return numberMember;
    }

    public ArrayList<String> getNumberHouse() {
        return numberHouse;
    }

    public void setNumberHouse(String numberHouse) {
        this.numberHouse.add(numberHouse);
    }

    public ArrayList<HouseName> getHouseName() {
        return houseName;
    }

    public void setHouseName(HouseName houseName) {
        this.houseName.add(houseName);
    }

    public String getNumFather() {
        return numFather;
    }

    public void setNumFather(String numFather) {
        this.numFather = numFather;
    }

    public String getNumMother() {
        return numMother;
    }

    public void setNumMother(String numMother) {
        this.numMother = numMother;
    }

    public String getNumSpouse() {
        return numSpouse;
    }

    public void setNumSpouse(String numSpouse) {
        this.numSpouse = numSpouse;
    }

    public String getNameFather() {
        return nameFather;
    }

    public void setNameFather(String nameFather) {
        this.nameFather = nameFather;
    }

    public String getNameMother() {
        return nameMother;
    }

    public void setNameMother(String nameMother) {
        this.nameMother = nameMother;
    }

    public String getNameSpouse() {
        return nameSpouse;
    }

    public void setNameSpouse(String nameSpouse) {
        this.nameSpouse = nameSpouse;
    }

    public void downloadInformationMember(final LoadCallback loadCallback) {
        Singleton.getSingletonNetwork().getCharacterMember(getNumberMember(), new SingletonNetwork.CharacterMemberLoadCallback() {
            @Override
            public void onLoad(CharacterMember characters) {
                setCharacterMember(characters);

                loadCallback.onLoad();
            }

            @Override
            public void onError(String error) {
                loadCallback.onError(error);
            }
        });

        /*Singleton.getSingleton().getCharacterMember(getNumberMember()).enqueue(new Callback<CharacterMember>() {
            @Override
            public void onResponse(Call<CharacterMember> call, Response<CharacterMember> response) {
                setCharacterMember(response.body());

                loadCallback.onLoad();
            }

            @Override
            public void onFailure(Call<CharacterMember> call, Throwable t) {
                loadCallback.onError(t.toString());
            }
        });*/
    }

    public void downloadCharactersName(final LoadCallback loadCallback, final int type, String numCharacters) {
        Singleton.getSingletonNetwork().getCharacterName(numCharacters, new SingletonNetwork.CharacterNameLoadCallback() {
            @Override
            public void onLoad(CharacterName charName) {
                switch (type) {
                    case Utils.FATHER:
                        setNameFather(charName.getName());
                        break;
                    case Utils.MOTHER:
                        setNameMother(charName.getName());
                        break;
                    case Utils.SPOUSE:
                        setNameSpouse(charName.getName());
                        break;
                }

                loadCallback.onLoad();
            }

            @Override
            public void onError(String error) {
                loadCallback.onError(error);
            }
        });

        /*Singleton.getSingleton().getCharacterName(numCharacters).enqueue(new Callback<CharacterName>() {
            @Override
            public void onResponse(Call<CharacterName> call, Response<CharacterName> response) {
                Log.d("Retrofit", "url = " + call.request().url());

                switch (type) {
                    case Utils.FATHER:
                        setNameFather(response.body().getName());
                        break;
                    case Utils.MOTHER:
                        setNameMother(response.body().getName());
                        break;
                    case Utils.SPOUSE:
                        setNameSpouse(response.body().getName());
                        break;
                }

                loadCallback.onLoad();
            }

            @Override
            public void onFailure(Call<CharacterName> call, Throwable t) {
                loadCallback.onError(t.toString());
            }
        });*/
    }

    public void downloadHouseName(final LoadCallback loadCallback) {
        final int[] temp = {0};

        for(int i = 0; i < getNumberHouse().size(); i++) {
            /*Singleton.getSingleton().getHouseName(getNumberHouse().get(i)).enqueue(new Callback<HouseName>() {
                @Override
                public void onResponse(Call<HouseName> call, Response<HouseName> response) {
                    Log.d("Retrofit", "url = " + call.request().url());
                    temp[0]++;

                    setHouseName(response.body());

                    if(temp[0] == getNumberHouse().size()) {
                        loadCallback.onLoad();
                    }
                }

                @Override
                public void onFailure(Call<HouseName> call, Throwable t) {
                    loadCallback.onError(t.toString());
                }
            });*/

            Singleton.getSingletonNetwork().getHouseName(getNumberHouse().get(i), new SingletonNetwork.HouseNameLoadCallback() {
                @Override
                public void onLoad(HouseName houseName) {
                    temp[0]++;

                    setHouseName(houseName);

                    if(temp[0] == getNumberHouse().size()) {
                        loadCallback.onLoad();
                    }
                }

                @Override
                public void onError(String error) {
                    loadCallback.onError(error);
                }
            });
        }
    }
}
