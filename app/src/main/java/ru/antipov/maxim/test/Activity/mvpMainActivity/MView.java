package ru.antipov.maxim.test.Activity.mvpMainActivity;

import java.util.ArrayList;

import ru.antipov.maxim.test.Parsers.Characters;

public interface MView {
    void addDataCharactersAdapter(ArrayList<Characters> characters);
    void initializeCharactersAdapter(ArrayList<Characters> characters);
    void updateCharactersAdapter(ArrayList<Characters> characters);
    void viewLoading(boolean visible);
    void toastMessage(String message);
    void prefMemberCharacterActivity(String numberMember);
}
