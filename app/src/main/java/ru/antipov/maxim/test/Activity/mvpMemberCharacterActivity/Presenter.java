package ru.antipov.maxim.test.Activity.mvpMemberCharacterActivity;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.test.Parsers.CharacterMember;
import ru.antipov.maxim.test.Parsers.CharacterName;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.Parsers.HouseName;
import ru.antipov.maxim.test.R;
import ru.antipov.maxim.test.Singleton.Singleton;
import ru.antipov.maxim.test.Utils.Utils;

public class Presenter {
    private Model model;
    private MView view;

    public Presenter(Model model) {this.model = model;}
    public void attachView(MView view) {this.view = view;}

    public void downloadInformationMember(final Context context) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        model.downloadInformationMember(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                if(model.getCharacterMember().getFather().length() != 0) {
                    model.setNumFather(Utils.getNumber(model.getCharacterMember().getFather()));
                    downloadCharactersName(context, model.getNumFather(), Utils.FATHER);
                } else {
                    view.viewLoadingFather(Utils.OFF_SHOW_LOADING);
                    view.fillingTextViewFather(model.getCharacterMember().getFather());
                }

                if(model.getCharacterMember().getMother().length() != 0) {
                    model.setNumMother(Utils.getNumber(model.getCharacterMember().getMother()));
                    downloadCharactersName(context, model.getNumMother(), Utils.MOTHER);
                } else {
                    view.viewLoadingMother(Utils.OFF_SHOW_LOADING);
                    view.fillingTextViewMother(model.getCharacterMember().getMother());
                }

                if(model.getCharacterMember().getSpouse().length() != 0) {
                    model.setNumSpouse(Utils.getNumber(model.getCharacterMember().getSpouse()));
                    downloadCharactersName(context, model.getNumSpouse(), Utils.SPOUSE);
                } else {
                    view.viewLoadingSpouse(Utils.OFF_SHOW_LOADING);
                    view.fillingTextViewSpouse(model.getCharacterMember().getSpouse());
                }

                view.fillingTextView(model.getCharacterMember().getName(), model.getCharacterMember().getGender(),
                        model.getCharacterMember().getCulture(), model.getCharacterMember().getBorn(),
                        model.getCharacterMember().getDied());

                view.fillingListView(model.getCharacterMember().getTitles(), model.getCharacterMember().getAliases(),
                        model.getCharacterMember().getTvSeries(), model.getCharacterMember().getPlayedBy());

                if(model.getCharacterMember().getAllegiances().size() != 0) {
                    for (int i = 0; i < model.getCharacterMember().getAllegiances().size(); i++) {
                        model.setNumberHouse(Utils.getNumber(model.getCharacterMember().getAllegiances().get(i)));
                    }

                    downloadHouseName(context);
                } else {
                    view.fillingAllegiances(model.getCharacterMember().getAllegiances());
                    view.viewLoadingAllegiances(Utils.OFF_SHOW_LOADING);
                }

                view.viewLoading(Utils.OFF_SHOW_LOADING);
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        });
    }

    public void downloadCharactersName(final Context context, String numCharacters, final int type) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        switch (type) {
            case Utils.FATHER:
                view.viewLoadingFather(Utils.ON_SHOW_LOADING);
                break;
            case Utils.MOTHER:
                view.viewLoadingMother(Utils.ON_SHOW_LOADING);
                break;
            case Utils.SPOUSE:
                view.viewLoadingSpouse(Utils.ON_SHOW_LOADING);
                break;
        }

        model.downloadCharactersName(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                switch (type) {
                    case Utils.FATHER:
                        view.viewLoadingFather(Utils.OFF_SHOW_LOADING);
                        view.fillingTextViewFather(model.getNameFather());
                        break;
                    case Utils.MOTHER:
                        view.viewLoadingMother(Utils.OFF_SHOW_LOADING);
                        view.fillingTextViewMother(model.getNameMother());
                        break;
                    case Utils.SPOUSE:
                        view.viewLoadingSpouse(Utils.OFF_SHOW_LOADING);
                        view.fillingTextViewSpouse(model.getNameSpouse());
                        break;
                }
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        }, type, numCharacters);
    }

    public void downloadHouseName(final Context context) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        model.downloadHouseName(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                final ArrayList<String> nameHouse = new ArrayList<>();

                for(int i = 0; i < model.getHouseName().size(); i++) {
                    nameHouse.add(model.getHouseName().get(i).getName());
                }

                view.fillingAllegiances(nameHouse);
                view.viewLoadingAllegiances(Utils.OFF_SHOW_LOADING);
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        });
    }

    public void clickTvFather() {
        view.prefMoreInfoCharacter(model.getNumFather());
    }

    public void clickTvMother() {
        view.prefMoreInfoCharacter(model.getNumMother());
    }

    public void clickTvSpouse() {
        view.prefMoreInfoCharacter(model.getNumSpouse());
    }
}
