package ru.antipov.maxim.test.Api;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.antipov.maxim.test.Parsers.CharacterMember;
import ru.antipov.maxim.test.Parsers.CharacterName;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.Parsers.HouseName;

public interface ApiFunction {
    @GET("api/houses/{numHouse}")
    Call<HouseName> getHouseName(@Path("numHouse") String numHouse);

    @GET("api/characters/{numMember}")
    Call<CharacterMember> getCharacterMember(@Path("numMember") String numMember);

    @GET("api/characters/{numMember}")
    Call<CharacterName> getCharacterName(@Path("numMember") String numMember);

    @GET("api/characters")
    Call<ArrayList<Characters>> getCharacters(@Query("page") String page,
                                              @Query("pageSize") String pageSize);
}