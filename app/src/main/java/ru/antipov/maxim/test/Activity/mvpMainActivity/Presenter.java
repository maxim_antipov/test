package ru.antipov.maxim.test.Activity.mvpMainActivity;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.R;
import ru.antipov.maxim.test.Singleton.Singleton;
import ru.antipov.maxim.test.Utils.Utils;

public class Presenter {
    private Model model;
    private MView view;

    public Presenter(Model model) {this.model = model;}
    public void attachView(MView view) {this.view = view;}

    public void initializeCharactersAdapter(final Context context) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        view.viewLoading(Utils.ON_SHOW_LOADING);

        model.initializeCharactersAdapter(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                view.initializeCharactersAdapter(model.getCharacters());
                view.viewLoading(Utils.OFF_SHOW_LOADING);
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        });
    }

    public void addData(final Context context) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        model.addData(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                view.addDataCharactersAdapter(model.getLastPage());
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        });
    }

    public void updateCharactersAdapter(final Context context) {
        if(!Utils.hasConnection(context)) {
            view.toastMessage(context.getString(R.string.notInternet));
            return;
        }

        view.viewLoading(Utils.ON_SHOW_LOADING);

        model.updateCharactersAdapter(new Model.LoadCallback() {
            @Override
            public void onLoad() {
                view.updateCharactersAdapter(model.getCharacters());
                view.viewLoading(Utils.OFF_SHOW_LOADING);
            }

            @Override
            public void onError(String error) {
                Log.e("Network Error", "error = " + error);
                view.toastMessage(context.getString(R.string.err_load_data));
            }
        });
    }

    public void clickBtnMoreInfo(int position) {
        view.prefMemberCharacterActivity(String.valueOf(position + 1));
    }
}
