package ru.antipov.maxim.test.Singleton;

import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import ru.antipov.maxim.test.Parsers.CharacterMember;
import ru.antipov.maxim.test.Parsers.CharacterName;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.Parsers.HouseName;

public class SingletonNetwork {
    public interface HouseNameLoadCallback {
        void onLoad(HouseName houseName);
        void onError(String error);
    }

    public interface CharactersLoadCallback {
        void onLoad(ArrayList<Characters> characters);
        void onError(String error);
    }

    public interface CharacterMemberLoadCallback {
        void onLoad(CharacterMember characters);
        void onError(String error);
    }

    public interface CharacterNameLoadCallback {
        void onLoad(CharacterName charName);
        void onError(String error);
    }

    public interface DownloadCallback<T> {
        interface Progress {
            int ERROR = -1;
            int CONNECT_SUCCESS = 0;
            int GET_INPUT_STREAM_SUCCESS = 1;
            int PROCESS_INPUT_STREAM_IN_PROGRESS = 2;
            int PROCESS_INPUT_STREAM_SUCCESS = 3;
        }

        void updateFromDownload(T result);
        NetworkInfo getActiveNetworkInfo();
        void onProgressUpdate(int progressCode, int percentComplete);
        void finishDownloading();
    }

    private String BaseUrl;
    private DownloadTask mDownloadTask;

    private final String tailHouseName       = "api/houses/";
    private final String tailCharacterMember = "api/characters/";
    private final String tailCharacterName   = "api/characters/";
    private final String tailCharacters      = "api/characters";


    //@GET("{numMember}")
    //Call<CharacterName> getCharacterName(@Path("numMember") String numMember);

    SingletonNetwork(String baseUrl) {
        BaseUrl = baseUrl;
    }

    public void getCharacterName(String numMember, final CharacterNameLoadCallback characterNameLoadCallback) {
        String mUrlString = BaseUrl + tailCharacterName + numMember;

        mDownloadTask = new DownloadTask(new DownloadCallback<String>() {
            @Override
            public void updateFromDownload(String result) {
                if(result != null) {
                    CharacterName charName = new CharacterName();

                    JsonElement jelement = new JsonParser().parse(result);
                    JsonObject jobject = jelement.getAsJsonObject();

                    String name = jobject.getAsJsonPrimitive("name").getAsString();

                    charName.setName(name);

                    characterNameLoadCallback.onLoad(charName);
                } else {
                    characterNameLoadCallback.onError("An error occurred during the download.");
                }
            }

            @Override
            public NetworkInfo getActiveNetworkInfo() {
                return null;
            }

            @Override
            public void onProgressUpdate(int progressCode, int percentComplete) {

            }

            @Override
            public void finishDownloading() {

            }
        });

        mDownloadTask.execute(mUrlString);
    }

    public void getCharacterMember(String numMember, final CharacterMemberLoadCallback characterMemberLoadCallback) {
        String mUrlString = BaseUrl + tailCharacterMember + numMember;

        mDownloadTask = new DownloadTask(new DownloadCallback<String>() {
            @Override
            public void updateFromDownload(String result) {
                if(result != null) {
                    CharacterMember character = new CharacterMember();

                    JsonElement jelement = new JsonParser().parse(result);
                    JsonObject jobject = jelement.getAsJsonObject();

                    String url = jobject.getAsJsonPrimitive("url").getAsString();
                    String name = jobject.getAsJsonPrimitive("name").getAsString();
                    String gender = jobject.getAsJsonPrimitive("gender").getAsString();
                    String culture = jobject.getAsJsonPrimitive("culture").getAsString();
                    String born = jobject.getAsJsonPrimitive("born").getAsString();
                    String died = jobject.getAsJsonPrimitive("died").getAsString();

                    //JsonArray jtitles = jobject.getAsJsonPrimitive("titles").getAsJsonArray()

                    JsonElement etitles = jobject.get("titles");
                    JsonArray jtitles = etitles.getAsJsonArray();

                    ArrayList<String> titles = new ArrayList<>();

                    for(int i = 0; i < jtitles.size(); i++) {
                        titles.add(jtitles.get(i).getAsString());
                    }

                    JsonElement ealiases = jobject.get("aliases");
                    JsonArray jaliases = ealiases.getAsJsonArray();

                    ArrayList<String> aliases = new ArrayList<>();

                    for(int i = 0; i < jaliases.size(); i++) {
                        aliases.add(jaliases.get(i).getAsString());
                    }

                    String father = jobject.getAsJsonPrimitive("father").getAsString();
                    String mother = jobject.getAsJsonPrimitive("mother").getAsString();
                    String spouse = jobject.getAsJsonPrimitive("spouse").getAsString();

                    JsonElement eallegiances = jobject.get("allegiances");
                    JsonArray jallegiances = eallegiances.getAsJsonArray();

                    ArrayList<String> allegiances = new ArrayList<>();

                    for(int i = 0; i < jallegiances.size(); i++) {
                        allegiances.add(jallegiances.get(i).getAsString());
                    }

                    JsonElement etvseries = jobject.get("tvSeries");
                    JsonArray jtvseries = etvseries.getAsJsonArray();

                    ArrayList<String> tvseries = new ArrayList<>();

                    for(int i = 0; i < jtvseries.size(); i++) {
                        tvseries.add(jtvseries.get(i).getAsString());
                    }

                    JsonElement eplayedby = jobject.get("playedBy");
                    JsonArray jplayedby = eplayedby.getAsJsonArray();

                    ArrayList<String> playedby = new ArrayList<>();

                    for(int i = 0; i < jplayedby.size(); i++) {
                        playedby.add(jplayedby.get(i).getAsString());
                    }

                    character.setUrl(url);
                    character.setName(name);
                    character.setGender(gender);
                    character.setCulture(culture);
                    character.setBorn(born);
                    character.setDied(died);
                    character.setTitles(titles);
                    character.setAliases(aliases);
                    character.setFather(father);
                    character.setMother(mother);
                    character.setSpouse(spouse);
                    character.setAllegiances(allegiances);
                    character.setTvSeries(tvseries);
                    character.setPlayedBy(playedby);

                    characterMemberLoadCallback.onLoad(character);
                } else {
                    characterMemberLoadCallback.onError("An error occurred during the download.");
                }
            }

            @Override
            public NetworkInfo getActiveNetworkInfo() {
                return null;
            }

            @Override
            public void onProgressUpdate(int progressCode, int percentComplete) {

            }

            @Override
            public void finishDownloading() {

            }
        });

        mDownloadTask.execute(mUrlString);
    }

    public void getCharacters(String page, String pageSize, final CharactersLoadCallback charactersLoadCallback) {
        String mUrlString = BaseUrl + tailCharacters + "?page=" + page + "&pageSize=" + pageSize;

        mDownloadTask = new DownloadTask(new DownloadCallback<String>() {
            @Override
            public void updateFromDownload(String result) {
                if(result != null) {
                    ArrayList<Characters> characters = new ArrayList<>();

                    JsonElement jelement = new JsonParser().parse(result);
                    JsonArray jarray = jelement.getAsJsonArray();

                    for(int i = 0; i < jarray.size(); i++) {
                        Characters charMember = new Characters();

                        String url = jarray.get(i).getAsJsonObject().getAsJsonPrimitive("url").getAsString();
                        String name = jarray.get(i).getAsJsonObject().getAsJsonPrimitive("name").getAsString();
                        String gender = jarray.get(i).getAsJsonObject().getAsJsonPrimitive("gender").getAsString();
                        String culture = jarray.get(i).getAsJsonObject().getAsJsonPrimitive("culture").getAsString();
                        String born = jarray.get(i).getAsJsonObject().getAsJsonPrimitive("born").getAsString();

                        charMember.setUrl(url);
                        charMember.setName(name);
                        charMember.setGender(gender);
                        charMember.setCulture(culture);
                        charMember.setBorn(born);

                        characters.add(charMember);
                    }

                    charactersLoadCallback.onLoad(characters);
                } else {
                    charactersLoadCallback.onError("An error occurred during the download.");
                }
            }

            @Override
            public NetworkInfo getActiveNetworkInfo() {
                return null;
            }

            @Override
            public void onProgressUpdate(int progressCode, int percentComplete) {

            }

            @Override
            public void finishDownloading() {

            }
        });

        mDownloadTask.execute(mUrlString);
    }

    public void getHouseName(String numHouse, final HouseNameLoadCallback houseNameLoadCallback) {
        String mUrlString = BaseUrl + tailHouseName + numHouse;

        mDownloadTask = new DownloadTask(new DownloadCallback<String>() {
            @Override
            public void updateFromDownload(String result) {
                if(result != null) {
                    HouseName houseName = new HouseName();

                    JsonElement jelement = new JsonParser().parse(result);
                    JsonObject jobject = jelement.getAsJsonObject();

                    String url  = jobject.getAsJsonPrimitive("url").getAsString();
                    String name = jobject.getAsJsonPrimitive("name").getAsString();

                    houseName.setUrl(url);
                    houseName.setName(name);

                    houseNameLoadCallback.onLoad(houseName);
                } else {
                    houseNameLoadCallback.onError("An error occurred during the download.");
                }
            }

            @Override
            public NetworkInfo getActiveNetworkInfo() {
                return null;
            }

            @Override
            public void onProgressUpdate(int progressCode, int percentComplete) {

            }

            @Override
            public void finishDownloading() {

            }
        });

        mDownloadTask.execute(mUrlString);
    }
}

class DownloadTask extends AsyncTask<String, Integer, DownloadTask.Result> {

    private SingletonNetwork.DownloadCallback<String> mCallback;

    DownloadTask(SingletonNetwork.DownloadCallback<String> callback) {
        setCallback(callback);
    }

    void setCallback(SingletonNetwork.DownloadCallback<String> callback) {
        mCallback = callback;
    }

    static class Result {
        public String mResultValue;
        public Exception mException;
        public Result(String resultValue) {
            mResultValue = resultValue;
        }
        public Result(Exception exception) {
            mException = exception;
        }
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected DownloadTask.Result doInBackground(String... urls) {
        Result result = null;
        if (!isCancelled() && urls != null && urls.length > 0) {
            String urlString = urls[0];
            try {
                URL url = new URL(urlString);
                String resultString = downloadUrl(url);
                if (resultString != null) {
                    result = new Result(resultString);
                } else {
                    throw new IOException("No response received.");
                }
            } catch(Exception e) {
                result = new Result(e);
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        if (result != null && mCallback != null) {
            if (result.mException != null) {
                mCallback.updateFromDownload(result.mException.getMessage());
            } else if (result.mResultValue != null) {
                mCallback.updateFromDownload(result.mResultValue);
            }
            mCallback.finishDownloading();
        }
    }

    @Override
    protected void onCancelled(Result result) {
    }

    private String downloadUrl(URL url) throws IOException {
        InputStream stream = null;
        HttpsURLConnection connection = null;
        String result = null;
        try {
            connection = (HttpsURLConnection) url.openConnection();
            connection.setReadTimeout(3000);
            connection.setConnectTimeout(3000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            publishProgress(SingletonNetwork.DownloadCallback.Progress.CONNECT_SUCCESS);
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }
            stream = connection.getInputStream();
            publishProgress(SingletonNetwork.DownloadCallback.Progress.GET_INPUT_STREAM_SUCCESS, 0);
            if (stream != null) {
                result = readStream(stream, 1000000);
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    public String readStream(InputStream stream, int maxReadSize)
            throws IOException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;
        StringBuffer buffer = new StringBuffer();
        while (((readSize = reader.read(rawBuffer)) != -1) && maxReadSize > 0) {
            if (readSize > maxReadSize) {
                readSize = maxReadSize;
            }
            buffer.append(rawBuffer, 0, readSize);
            maxReadSize -= readSize;
        }
        return buffer.toString();
    }
}
