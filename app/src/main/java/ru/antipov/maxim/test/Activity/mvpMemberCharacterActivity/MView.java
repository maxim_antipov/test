package ru.antipov.maxim.test.Activity.mvpMemberCharacterActivity;

import java.util.ArrayList;

import ru.antipov.maxim.test.Parsers.CharacterMember;

public interface MView {
    void viewLoading(boolean visible);
    void viewLoadingAllegiances(boolean visible);
    void viewLoadingFather(boolean visible);
    void viewLoadingMother(boolean visible);
    void viewLoadingSpouse(boolean visible);
    void toastMessage(String message);
    void fillingTextView(String name, String gender, String culture, String born, String died);
    void fillingTextViewFather(String father);
    void fillingTextViewMother(String mother);
    void fillingTextViewSpouse(String spouse);
    void fillingListView(ArrayList<String> titles, ArrayList<String> aliases, ArrayList<String> tvSeries, ArrayList<String> playedBy);
    void fillingAllegiances(ArrayList<String> allegiances);
    void prefMoreInfoCharacter(String numCharacter);
}
