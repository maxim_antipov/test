package ru.antipov.maxim.test.Parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CharacterMember {
    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("culture")
    @Expose
    private String culture;

    @SerializedName("born")
    @Expose
    private String born;

    @SerializedName("died")
    @Expose
    private String died;

    @SerializedName("titles")
    @Expose
    private ArrayList<String> titles;

    @SerializedName("aliases")
    @Expose
    private ArrayList<String> aliases;

    @SerializedName("father")
    @Expose
    private String father;

    @SerializedName("mother")
    @Expose
    private String mother;

    @SerializedName("spouse")
    @Expose
    private String spouse;

    @SerializedName("allegiances")
    @Expose
    private ArrayList<String> allegiances;

    /*@SerializedName("books")
    @Expose
    private ArrayList<String> books;

    @SerializedName("povBooks")
    @Expose
    private ArrayList<String> povBooks;*/

    @SerializedName("tvSeries")
    @Expose
    private ArrayList<String> tvSeries;

    @SerializedName("playedBy")
    @Expose
    private ArrayList<String> playedBy;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<String> titles) {
        this.titles = titles;
    }

    public ArrayList<String> getAliases() {
        return aliases;
    }

    public void setAliases(ArrayList<String> aliases) {
        this.aliases = aliases;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public ArrayList<String> getAllegiances() {
        return allegiances;
    }

    public void setAllegiances(ArrayList<String> allegiances) {
        this.allegiances = allegiances;
    }

    public ArrayList<String> getTvSeries() {
        return tvSeries;
    }

    /*public ArrayList<String> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<String> books) {
        this.books = books;
    }

    public ArrayList<String> getPovBooks() {
        return povBooks;
    }

    public void setPovBooks(ArrayList<String> povBooks) {
        this.povBooks = povBooks;
    }*/

    public void setTvSeries(ArrayList<String> tvSeries) {
        this.tvSeries = tvSeries;
    }

    public ArrayList<String> getPlayedBy() {
        return playedBy;
    }

    public void setPlayedBy(ArrayList<String> playedBy) {
        this.playedBy = playedBy;
    }
}
