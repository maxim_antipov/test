package ru.antipov.maxim.test.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;

public class Utils {
    static final public boolean ON_SHOW_LOADING = true;
    static final public boolean OFF_SHOW_LOADING = false;
    static final public int FATHER = 0;
    static final public int MOTHER = 1;
    static final public int SPOUSE = 2;

    public static boolean hasConnection(final Context context) {
        if(context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        return false;
    }

    public static String getNumber(String url) {
        int temp = 0;

        for(int i = url.length() - 1; i > 0; i--) {
            if(url.charAt(i) == '/') {
                temp = i + 1;
                break;
            }
        }

        return url.substring(temp);
    }

    public static SpannableString getEmphasizeText(String text) {
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new UnderlineSpan(), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return ss;
    }
}
