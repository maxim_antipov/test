package ru.antipov.maxim.test.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.R;

public class CharactersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM    = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private ArrayList<Characters> itemsCharacters = new ArrayList<>();
    private boolean isLoading = false;

    private Context context;

    public interface setOnClickListenerBtnMoreInfo {
        void onClickMoreInfo(int position);
    }

    public interface setOnBeginLoading {
        void beginLoading();
    }

    private setOnClickListenerBtnMoreInfo setOnClick;
    private setOnBeginLoading             setLoading;

    public void setOnClickListenerRVAdepter(setOnClickListenerBtnMoreInfo setOnClick) {
        this.setOnClick = setOnClick;
    }

    public void setOnBeginLoadingRVAdapter(setOnBeginLoading setLoading) {
        this.setLoading = setLoading;
    }

    public void addIndexCharacters(int index, ArrayList<Characters> items) {
        this.itemsCharacters.addAll(index, items);
    }

    public void addCharacters(ArrayList<Characters> items) {
        this.itemsCharacters.addAll(items);
    }

    public void clearItemsCharacters() {
        itemsCharacters.clear();
    }

    public CharactersAdapter(ArrayList<Characters> items, final RecyclerView rvItem, Context context) {
        this.itemsCharacters.addAll(items);
        this.context = context;

        rvItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount  = ((LinearLayoutManager) recyclerView.getLayoutManager()).getChildCount();
                int totalItemCount    = ((LinearLayoutManager) recyclerView.getLayoutManager()).getItemCount();
                int firstVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if(!isLoading) {
                    if(firstVisibleItems != 0 && (visibleItemCount + firstVisibleItems) >= totalItemCount) {
                        CharactersAdapter.this.itemsCharacters.add(null);

                        recyclerView.post(new Runnable() {
                            public void run() {
                                notifyItemInserted(CharactersAdapter.this.itemsCharacters.size() - 1);
                            }
                        });

                        isLoading = true;
                        setLoading.beginLoading();
                    }
                }
            }
        });
    }

    public void removeNull() {
        if(itemsCharacters.size() > 0) {
            isLoading = false;
            itemsCharacters.remove(itemsCharacters.size() - 1);
            notifyItemRemoved(itemsCharacters.size());
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarItemLoading);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvGender;
        public TextView tvCulture;
        public TextView tvBorn;
        public Button btnInfo;

        ItemViewHolder(View itemView) {
            super(itemView);

            tvName    = (TextView)    itemView.findViewById(R.id.tvName);
            tvGender  = (TextView)    itemView.findViewById(R.id.tvGender);
            tvCulture = (TextView)    itemView.findViewById(R.id.tvCulture);
            tvBorn    = (TextView)    itemView.findViewById(R.id.tvBorn);
            btnInfo   = (Button)      itemView.findViewById(R.id.btnInfo);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_characters, parent, false);
            return new ItemViewHolder(v);
        } else if(viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.tvName.setText(context.getString(R.string.name)       + (itemsCharacters.get(position).getName().length()    == 0 ? "-" : itemsCharacters.get(position).getName()));
            itemViewHolder.tvCulture.setText(context.getString(R.string.culture) + (itemsCharacters.get(position).getCulture().length() == 0 ? "-" : itemsCharacters.get(position).getCulture().length()));
            itemViewHolder.tvBorn.setText(context.getString(R.string.born)       + (itemsCharacters.get(position).getBorn().length()    == 0 ? "-" : itemsCharacters.get(position).getBorn().length()));
            itemViewHolder.tvGender.setText(context.getString(R.string.gender)   + (itemsCharacters.get(position).getGender().length()  == 0 ? "-" : itemsCharacters.get(position).getGender().length()));

            itemViewHolder.btnInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setOnClick.onClickMoreInfo(position);
                }
            });

        } else if(holder instanceof  LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public int getItemViewType(int position) {
        return itemsCharacters.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemsCharacters == null ? 0 : itemsCharacters.size();
    }
}
