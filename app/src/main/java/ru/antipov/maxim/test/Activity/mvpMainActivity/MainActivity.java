package ru.antipov.maxim.test.Activity.mvpMainActivity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.test.Activity.mvpMemberCharacterActivity.MemberCharacterActivity;
import ru.antipov.maxim.test.Adapter.CharactersAdapter;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.R;
import ru.antipov.maxim.test.Singleton.Singleton;
import ru.antipov.maxim.test.Utils.Utils;

public class MainActivity extends AppCompatActivity implements MView {
    private RecyclerView rvCharacters;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View loading;

    private CharactersAdapter mCharactersAdapter;

    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new Presenter(new Model());
        presenter.attachView(this);

        init();

        presenter.initializeCharactersAdapter(getApplicationContext());
    }

    private void init() {
        rvCharacters        = (RecyclerView) findViewById(R.id.rvCharacters);
        loading             = findViewById(R.id.loadingMain);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);

        rvCharacters.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                presenter.updateCharactersAdapter(getApplicationContext());
            }
        });
    }

    @Override
    public void addDataCharactersAdapter(ArrayList<Characters> characters) {
        if(mCharactersAdapter != null) {
            mCharactersAdapter.removeNull();

            mCharactersAdapter.addIndexCharacters(mCharactersAdapter.getItemCount(), characters);
            mCharactersAdapter.notifyItemRangeChanged(mCharactersAdapter.getItemCount(), characters.size());
        }
    }

    @Override
    public void initializeCharactersAdapter(ArrayList<Characters> characters) {

        if(mCharactersAdapter == null) {
            mCharactersAdapter = new CharactersAdapter(characters, rvCharacters, getApplicationContext());

            mCharactersAdapter.setOnClickListenerRVAdepter(new CharactersAdapter.setOnClickListenerBtnMoreInfo() {
                @Override
                public void onClickMoreInfo(int position) {
                    presenter.clickBtnMoreInfo(position);
                }
            });

            mCharactersAdapter.setOnBeginLoadingRVAdapter(new CharactersAdapter.setOnBeginLoading() {
                @Override
                public void beginLoading() {
                    presenter.addData(getApplicationContext());
                }
            });

            rvCharacters.setAdapter(mCharactersAdapter);
        }
    }

    @Override
    public void updateCharactersAdapter(ArrayList<Characters> characters) {
        if(mCharactersAdapter != null) {
            mCharactersAdapter.clearItemsCharacters();

            mCharactersAdapter.addCharacters(characters);
            mCharactersAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void viewLoading(boolean visible) {
        if(visible) {
            loading.setVisibility(View.VISIBLE);
            rvCharacters.setVisibility(View.GONE);
        } else {
            loading.setVisibility(View.GONE);
            rvCharacters.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void prefMemberCharacterActivity(String numberMember) {
        Intent intent = new Intent(this, MemberCharacterActivity.class);
        intent.putExtra(MemberCharacterActivity.NUMBER_MEMBER, numberMember);
        startActivity(intent);
    }
}

