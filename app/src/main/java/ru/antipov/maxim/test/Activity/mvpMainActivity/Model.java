package ru.antipov.maxim.test.Activity.mvpMainActivity;

import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.test.Parsers.Characters;
import ru.antipov.maxim.test.Singleton.Singleton;
import ru.antipov.maxim.test.Singleton.SingletonNetwork;

public class Model {
    public interface LoadCallback {
        void onLoad();
        void onError(String error);
    }

    private ArrayList<Characters> mCharacters;
    private int page;
    private int pageSize;

    Model() {
        page = 1;
        pageSize = 50;

        mCharacters = new ArrayList<>();
    }

    public ArrayList<Characters> getCharacters() {
        return mCharacters;
    }

    public void setCharacters(ArrayList<Characters> characters) {
        mCharacters.addAll(characters);
    }

    public String getPage() {
        return String.valueOf(page);
    }

    public String getPageSize() {
        return String.valueOf(pageSize);
    }

    public void nextPages() {
        page += 1;
    }

    public ArrayList<Characters> getLastPage() {
        int firstIndex  = ((page - 2) * pageSize);
        int secondIndex = firstIndex + pageSize;

        return new ArrayList<>(mCharacters.subList(firstIndex, secondIndex));
    }

    public void clearPages() {
        page = 1;
        mCharacters.clear();
    }

    public void initializeCharactersAdapter(final LoadCallback loadCallback) {
        Singleton.getSingletonNetwork().getCharacters(getPage(), getPageSize(), new SingletonNetwork.CharactersLoadCallback() {
            @Override
            public void onLoad(ArrayList<Characters> characters) {
                setCharacters(characters);
                nextPages();

                loadCallback.onLoad();
            }

            @Override
            public void onError(String error) {
                loadCallback.onError(error);
            }
        });

        /*Singleton.getSingleton().getCharacters(getPage(), getPageSize()).enqueue(new Callback<ArrayList<Characters>>() {
            @Override
            public void onResponse(Call<ArrayList<Characters>> call, Response<ArrayList<Characters>> response) {
                Log.d("Retrofit", "url = " + call.request().url());

                setCharacters(response.body());
                nextPages();

                loadCallback.onLoad();
            }

            @Override
            public void onFailure(Call<ArrayList<Characters>> call, Throwable t) {
               loadCallback.onError(t.toString());
            }
        });*/
    }

    public void addData(final LoadCallback loadCallback) {
        Singleton.getSingletonNetwork().getCharacters(getPage(), getPageSize(), new SingletonNetwork.CharactersLoadCallback() {
            @Override
            public void onLoad(ArrayList<Characters> characters) {
                setCharacters(characters);
                nextPages();

                loadCallback.onLoad();
            }

            @Override
            public void onError(String error) {
                loadCallback.onError(error);
            }
        });

        /*Singleton.getSingleton().getCharacters(getPage(), getPageSize()).enqueue(new Callback<ArrayList<Characters>>() {
            @Override
            public void onResponse(Call<ArrayList<Characters>> call, Response<ArrayList<Characters>> response) {
                setCharacters(response.body());
                nextPages();

                loadCallback.onLoad();
            }

            @Override
            public void onFailure(Call<ArrayList<Characters>> call, Throwable t) {
                loadCallback.onError(t.toString());
            }
        });*/
    }

    public void updateCharactersAdapter(final LoadCallback loadCallback) {
        clearPages();

        Singleton.getSingletonNetwork().getCharacters(getPage(), getPageSize(), new SingletonNetwork.CharactersLoadCallback() {
            @Override
            public void onLoad(ArrayList<Characters> characters) {
                setCharacters(characters);
                nextPages();

                loadCallback.onLoad();
            }

            @Override
            public void onError(String error) {
                loadCallback.onError(error);
            }
        });

        /*Singleton.getSingleton().getCharacters(getPage(), getPageSize()).enqueue(new Callback<ArrayList<Characters>>() {
            @Override
            public void onResponse(Call<ArrayList<Characters>> call, Response<ArrayList<Characters>> response) {
                setCharacters(response.body());
                nextPages();
            }

            @Override
            public void onFailure(Call<ArrayList<Characters>> call, Throwable t) {
                loadCallback.onError(t.toString());
            }
        });*/
    }
}
