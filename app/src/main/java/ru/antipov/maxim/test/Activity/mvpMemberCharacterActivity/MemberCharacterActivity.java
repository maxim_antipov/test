package ru.antipov.maxim.test.Activity.mvpMemberCharacterActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import ru.antipov.maxim.test.R;
import ru.antipov.maxim.test.Utils.Utils;

public class MemberCharacterActivity extends AppCompatActivity implements MView {
    public final static String NUMBER_MEMBER = "NUMBER_MEMBER";

    private TextView tvName;
    private TextView tvGender;
    private TextView tvCulture;
    private TextView tvBorn;
    private TextView tvDied;
    private TextView tvTitles;
    private ListView lvTitles;
    private TextView tvAliases;
    private ListView lvAliases;
    private TextView father;
    private TextView mother;
    private TextView spouse;
    private TextView tvAllegiances;
    private ListView lvAllegiances;
    private TextView tvTvSeries;
    private ListView lvTvSeries;
    private TextView tvPlayedBy;
    private ListView lvPlayedBy;
    private RelativeLayout rlMember;
    private View loading;
    private View loadingFather;
    private View loadingMother;
    private View loadingSpouse;
    private View loadingAllegiances;

    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memeber_character);

        presenter = new Presenter(new Model(getIntent().getStringExtra(NUMBER_MEMBER)));
        presenter.attachView(this);

        init();

        presenter.downloadInformationMember(getApplicationContext());
    }

    private void init() {
        tvName             = (TextView)       findViewById(R.id.tvName);
        tvGender           = (TextView)       findViewById(R.id.tvGender);
        tvCulture          = (TextView)       findViewById(R.id.tvCulture);
        tvBorn             = (TextView)       findViewById(R.id.tvBorn);
        tvDied             = (TextView)       findViewById(R.id.tvDied);
        tvTitles           = (TextView)       findViewById(R.id.tvTitles);
        lvTitles           = (ListView)       findViewById(R.id.lvTitles);
        tvAliases          = (TextView)       findViewById(R.id.tvAliases);
        lvAliases          = (ListView)       findViewById(R.id.lvAliases);
        father             = (TextView)       findViewById(R.id.father);
        mother             = (TextView)       findViewById(R.id.mother);
        spouse             = (TextView)       findViewById(R.id.spouse);
        tvAllegiances      = (TextView)       findViewById(R.id.tvAllegiances);
        lvAllegiances      = (ListView)       findViewById(R.id.lvAllegiances);
        tvTvSeries         = (TextView)       findViewById(R.id.tvTvSeries);
        lvTvSeries         = (ListView)       findViewById(R.id.lvTvSeries);
        tvPlayedBy         = (TextView)       findViewById(R.id.tvPlayedBy);
        lvPlayedBy         = (ListView)       findViewById(R.id.lvPlayedBy);
        rlMember           = (RelativeLayout) findViewById(R.id.rlMember);
        loading            =                  findViewById(R.id.loadingMemberCharacter);
        loadingFather      =                  findViewById(R.id.loadingFather);
        loadingMother      =                  findViewById(R.id.loadingMother);
        loadingSpouse      =                  findViewById(R.id.loadingSpouse);
        loadingAllegiances =                  findViewById(R.id.loadingAllegiances);
    }

    @Override
    public void viewLoading(boolean visible) {
        if(visible) {
            loading.setVisibility(View.VISIBLE);
            rlMember.setVisibility(View.GONE);
        } else {
            loading.setVisibility(View.GONE);
            rlMember.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void viewLoadingAllegiances(boolean visible) {
        if(visible) {
            loadingAllegiances.setVisibility(View.VISIBLE);
            lvAllegiances.setVisibility(View.GONE);
        } else {
            loadingAllegiances.setVisibility(View.GONE);
            lvAllegiances.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void viewLoadingFather(boolean visible) {
        if(visible) {
            loadingFather.setVisibility(View.VISIBLE);
        } else {
            loadingFather.setVisibility(View.GONE);
        }
    }

    @Override
    public void viewLoadingMother(boolean visible) {
        if(visible) {
            loadingMother.setVisibility(View.VISIBLE);
        } else {
            loadingMother.setVisibility(View.GONE);
        }
    }

    @Override
    public void viewLoadingSpouse(boolean visible) {
        if(visible) {
            loadingSpouse.setVisibility(View.VISIBLE);
        } else {
            loadingSpouse.setVisibility(View.GONE);
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void fillingTextViewFather(String father) {
        this.father.setText((father.length()  == 0 ? "-" : Utils.getEmphasizeText(father)));

        if(father.length() != 0) {
            this.father.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.clickTvFather();
                }
            });
        }
    }

    @Override
    public void fillingTextViewMother(String mother) {
        this.mother.setText((mother.length()  == 0 ? "-" : Utils.getEmphasizeText(mother)));

        if(mother.length() != 0) {
            this.mother.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.clickTvMother();
                }
            });
        }
    }

    @Override
    public void fillingTextViewSpouse(String spouse) {
        this.spouse.setText((spouse.length()  == 0 ? "-" : Utils.getEmphasizeText(spouse)));

        if(spouse.length() != 0) {
            this.spouse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.clickTvSpouse();
                }
            });
        }
    }

    @Override
    public void fillingTextView(String name, String gender, String culture, String born, String died) {
        tvName.setText(getString(R.string.name)       + (name.length()    == 0 ? "-" : name));
        tvGender.setText(getString(R.string.gender)   + (gender.length()  == 0 ? "-" : gender));
        tvCulture.setText(getString(R.string.culture) + (culture.length() == 0 ? "-" : culture));
        tvBorn.setText(getString(R.string.born)       + (born.length()    == 0 ? "-" : born));
        tvDied.setText(getString(R.string.died)       + (died.length()    == 0 ? "-" : died));
    }

    @Override
    public void fillingListView(ArrayList<String> titles, ArrayList<String> aliases, ArrayList<String> tvSeries, ArrayList<String> playedBy) {
        if(titles.get(0).length() != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_adapter, titles);
            lvTitles.setAdapter(adapter);
        } else {
            tvTitles.setText(getString(R.string.titles) + "-");
            lvTitles.setVisibility(View.GONE);
        }

        if(aliases.get(0).length() != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_adapter, aliases);
            lvAliases.setAdapter(adapter);
        } else {
            tvAliases.setText(getString(R.string.aliases) + "-");
            lvAliases.setVisibility(View.GONE);
        }

        if(tvSeries.get(0).length() != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_adapter, tvSeries);
            lvTvSeries.setAdapter(adapter);
        } else {
            tvTvSeries.setText(getString(R.string.tv_series) + "-");
            lvTvSeries.setVisibility(View.GONE);
        }

        if(playedBy.get(0).length() != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_adapter, playedBy);
            lvPlayedBy.setAdapter(adapter);
        } else {
            tvPlayedBy.setText(getString(R.string.played_by) + "-");
            lvPlayedBy.setVisibility(View.GONE);
        }
    }

    @Override
    public void fillingAllegiances(ArrayList<String> allegiances) {
        if(allegiances.size() != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_adapter, allegiances);
            lvAllegiances.setAdapter(adapter);
            lvAllegiances.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                }
            });
        } else {
            tvAllegiances.setText(getString(R.string.allegiances) + "-");
            lvAllegiances.setVisibility(View.GONE);
        }
    }

    @Override
    public void prefMoreInfoCharacter(String numCharacter) {
        Intent intent = new Intent(this, MemberCharacterActivity.class);
        intent.putExtra(MemberCharacterActivity.NUMBER_MEMBER, numCharacter);
        startActivity(intent);
    }
}
