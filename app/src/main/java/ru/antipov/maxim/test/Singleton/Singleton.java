package ru.antipov.maxim.test.Singleton;

import android.app.Application;
import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.antipov.maxim.test.Api.ApiFunction;

public class Singleton extends Application {
    //private static ApiFunction singleton;
    //private Retrofit retrofit;

    private static SingletonNetwork sSingletonNetwork;

    private final String BASE_URL = "https://anapioficeandfire.com/";

    @Override
    public void onCreate() {
        super.onCreate();

        sSingletonNetwork = new SingletonNetwork(BASE_URL);

        //retrofit = new Retrofit.Builder()
        //        .baseUrl(BASE_URL) //Базовая часть адреса
        //        .addConverterFactory(GsonConverterFactory.create())
        //        .build();

        //singleton = retrofit.create(ApiFunction.class);
    }

    //public static ApiFunction getSingleton() {
    //    return singleton;
    //}

    public static SingletonNetwork getSingletonNetwork() {
        return sSingletonNetwork;
    }
}
